package com.codebrat.androidtestscholarnex.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.codebrat.androidtestscholarnex.activities.MainActivity;
import com.codebrat.androidtestscholarnex.activities.SignUpActivity;

import java.util.HashMap;

/**
 * Created by Shikhar on 16-03-2017.
 */

public class SessionManager {
	private SharedPreferences mPref;
	private SharedPreferences.Editor mEditor;
	private Context mContext;

	public static final int PRIVATE_MODE = 0;

	// Sharedpref file name
	private static final String PREF_NAME = "ScholarnexPref";

	// All Shared Preferences Keys
	private static final String IS_LOGIN = "IsLoggedIn";
	public static final String KEY_NAME = "name";
	public static final String KEY_EMAIL = "email";
	public static final String KEY_PHONE = "phonenumber";

	// Constructor
	public SessionManager(Context context){
		this.mContext = context;
		mPref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		mEditor = mPref.edit();
	}

	/**
	 * Create login session
	 * */
	public void createLoginSession(String name, String email, String phone){
		mEditor.putBoolean(IS_LOGIN, true);
		mEditor.putString(KEY_NAME, name);
		mEditor.putString(KEY_EMAIL, email);
		mEditor.putString(KEY_PHONE, phone);

		// commit changes
		mEditor.commit();
	}

	public void checkLogin(){
		if(!this.isLoggedIn()){
			Intent i = new Intent(mContext, SignUpActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			mContext.startActivity(i);
			((Activity)mContext).finish();
		} else {
			Intent i = new Intent(mContext, MainActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			mContext.startActivity(i);
			((Activity)mContext).finish();
		}
	}



	/**
	 * Get stored session data
	 * */
	public HashMap<String, String> getUserDetails(){
		HashMap<String, String> user = new HashMap<String, String>();
		user.put(KEY_NAME, mPref.getString(KEY_NAME, null));
		user.put(KEY_EMAIL, mPref.getString(KEY_EMAIL, null));
		user.put(KEY_PHONE, mPref.getString(KEY_PHONE, null));
		return user;
	}

	public boolean deleteUserDetails(){
		mEditor.clear();
		mEditor.commit();
		return true;
	}

	/**
	 * Clear session details
	 * */
	public void logoutUser(){
		mEditor.clear();
		mEditor.commit();
		Intent i = new Intent(mContext, SignUpActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		mContext.startActivity(i);
		((Activity)mContext).finish();
	}

	/**
	 * Quick check for login
	 * **/
	public boolean isLoggedIn(){
		return mPref.getBoolean(IS_LOGIN, false);
	}
}
