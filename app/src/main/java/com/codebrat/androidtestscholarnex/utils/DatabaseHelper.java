package com.codebrat.androidtestscholarnex.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.codebrat.androidtestscholarnex.model.AttendeeDataModel;
import com.codebrat.androidtestscholarnex.model.EventDataModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Shikhar on 19-03-2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "events_database";

	// Events table name
	private static final String TABLE_EVENTS = "events";
	private static final String TABLE_ATTENDEES = "attendees";

	// Events Table Columns names
	private static final String KEY_EVENT_ID = "eventid";
	private static final String KEY_EVENT_NAME = "eventname";
	private static final String KEY_EVENT_DETAILS = "eventdetails";

	private static final String KEY_ATTENDEE_ID = "attendeeid";
	private static final String KEY_ATTENDEE_NAME = "attendeename";
	private static final String KEY_ATTENDEE_EVENT_NAME = "attendee_event_name";
	private static final String KEY_ATTENDENCE = "attendence";
	private static final String KEY_ATTENDENCE_NOTE = "attendence_note";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_EVENTS_TABLE = "CREATE TABLE " + TABLE_EVENTS + " ("
			+ KEY_EVENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ KEY_EVENT_NAME + " TEXT UNIQUE, "
			+ KEY_EVENT_DETAILS + " TEXT "
			+ ")";
		db.execSQL(CREATE_EVENTS_TABLE);

		String CREATE_ATTENDES_TABLE = "CREATE TABLE " + TABLE_ATTENDEES + " ("
			+ KEY_ATTENDEE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ KEY_ATTENDEE_NAME + " TEXT UNIQUE, "
			+ KEY_ATTENDEE_EVENT_NAME + " TEXT, "
			+ KEY_ATTENDENCE + " TEXT, "
			+ KEY_ATTENDENCE_NOTE + " TEXT "
			+ ")";
		db.execSQL(CREATE_ATTENDES_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ATTENDEES);
		// Create tables again
		onCreate(db);
	}

	/**
	 * Storing event details in database
	 * */
	public long addEvent(String eventName, String eventDetails) {
		long rowId;
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_EVENT_NAME, eventName);
		values.put(KEY_EVENT_DETAILS, eventDetails);

		// Inserting Row
		try {
			rowId = db.insertOrThrow(TABLE_EVENTS, null, values);
		} catch (SQLiteConstraintException sce){
			return -1;
		}
		db.close(); // Closing database connection
		return rowId;
	}

	/**
	 * Getting events data from database
	 * */
	public List<EventDataModel> getAllEventDetails(){
		List<EventDataModel> eventDataModelList = new ArrayList<>();
		String selectQuery = "SELECT * FROM " + TABLE_EVENTS;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		// Move to first row
		if(cursor.moveToFirst()){
			do {
				EventDataModel eventDataModel = new EventDataModel();
				eventDataModel.setEventId(cursor.getInt(cursor.getColumnIndex(KEY_EVENT_ID)));
				eventDataModel.setEventName(cursor.getString(cursor.getColumnIndex(KEY_EVENT_NAME)));
				eventDataModel.setEventDetails(cursor.getString(cursor.getColumnIndex(KEY_EVENT_DETAILS)));

				eventDataModelList.add(eventDataModel);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		// return event
		return eventDataModelList;
	}

	/**
	 * Getting event table status
	 * return true if rows are there in table
	 * */
	public int getEventsRowCount() {
		String countQuery = "SELECT * FROM " + TABLE_EVENTS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int rowCount = cursor.getCount();

		cursor.close();
		db.close();

		// return row count
		return rowCount;
	}

	public long addAttendee(String attendeeName, String eventName){
		long rowId;
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_ATTENDEE_NAME, attendeeName);
		values.put(KEY_ATTENDEE_EVENT_NAME, eventName);
		// Inserting Row
		try {
			rowId = db.insertOrThrow(TABLE_ATTENDEES, null, values);
		} catch (SQLiteConstraintException sce){
			return -1;
		}
		db.close(); // Closing database connection
		return rowId;
	}

	public void addAttendence(String attendeeName, String eventName, String attendence, String note){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_ATTENDENCE, attendence);
		values.put(KEY_ATTENDENCE_NOTE, note);
		// Updating Row
		db.update(TABLE_ATTENDEES, values, KEY_ATTENDEE_NAME + "=?" + " AND "
			+ KEY_ATTENDEE_EVENT_NAME + "=?", new String[]{attendeeName, eventName});
		db.close(); // Closing database connection
	}


	public List<AttendeeDataModel> getAttendeesDetails(String eventName){
		List<AttendeeDataModel> attendeeDataModelList = new ArrayList<>();
		SQLiteDatabase db = this.getReadableDatabase();
		String classQuery = "SELECT * FROM " + TABLE_ATTENDEES + " WHERE "
			+ KEY_ATTENDEE_EVENT_NAME + " = '" + eventName + "'" ;

		Cursor cursor = db.rawQuery(classQuery, null);
		// Move to first row
		if(cursor.moveToFirst()){
			do {
				AttendeeDataModel attendeeDataModel = new AttendeeDataModel();
				attendeeDataModel.setAttendeeName(cursor.getString(cursor.getColumnIndex(KEY_ATTENDEE_NAME)));
				attendeeDataModel.setEventName(cursor.getString(cursor.getColumnIndex(KEY_ATTENDEE_EVENT_NAME)));
				attendeeDataModel.setAttendence(cursor.getString(cursor.getColumnIndex(KEY_ATTENDENCE)));
				attendeeDataModel.setNote(cursor.getString(cursor.getColumnIndex(KEY_ATTENDENCE_NOTE)));
				attendeeDataModelList.add(attendeeDataModel);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		// return attendee
		return attendeeDataModelList;
	}

	public int getAttendeesRowCount() {
		String countQuery = "SELECT * FROM " + TABLE_ATTENDEES;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int rowCount = cursor.getCount();

		cursor.close();
		db.close();

		// return row count
		return rowCount;
	}

	/**
	 * Delete events tables
	 * */
	public void resetEventsTables(){
		SQLiteDatabase db = this.getWritableDatabase();
		// Delete All Rows
		db.delete(TABLE_EVENTS, null, null);
		db.close();
	}

	public void resetAttendeesTables(){
		SQLiteDatabase db = this.getWritableDatabase();
		// Delete All Rows
		db.delete(TABLE_ATTENDEES, null, null);
		db.close();
	}
}

