package com.codebrat.androidtestscholarnex.model;

/**
 * Created by Shikhar on 19-03-2017.
 */

public class AttendeeDataModel {
	private String mAttendeeName;
	private String mEventName;
	private String mAttendence;
	private String mNote;

	public AttendeeDataModel(String attendeeName, String eventName){
		mAttendeeName = attendeeName;
		mEventName = eventName;
	}

	public AttendeeDataModel(){

	}

	public String getAttendeeName(){
		return mAttendeeName;
	}

	public void setAttendeeName(String attendeeName){
		mAttendeeName = attendeeName;
	}

	public String getEventName(){
		return mEventName;
	}

	public void setEventName(String eventName){
		mEventName = eventName;
	}

	public String getAttendence(){
		return mAttendence;
	}

	public void setAttendence(String attendence){
		mAttendence = attendence;
	}

	public String getNote(){
		return mNote;
	}

	public void setNote(String note){
		mNote = note;
	}
}
