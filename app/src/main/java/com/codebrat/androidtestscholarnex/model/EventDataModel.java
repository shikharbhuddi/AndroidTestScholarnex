package com.codebrat.androidtestscholarnex.model;

/**
 * Created by Shikhar on 19-03-2017.
 */

public class EventDataModel {
	private int mEventId;
	private String mEventName;
	private String mEventDetails;

	public EventDataModel(int eventId, String eventName, String eventDetails){
		mEventId = eventId;
		mEventName = eventName;
		mEventDetails = eventDetails;
	}

	public EventDataModel(){

	}

	public int getEventId(){
		return mEventId;
	}

	public void setEventId(int eventId){
		mEventId = eventId;
	}

	public String getEventName(){
		return mEventName;
	}

	public void setEventName(String eventName){
		mEventName = eventName;
	}

	public String getEventDetails(){
		return mEventDetails;
	}

	public void setEventDetails(String eventDetails){
		mEventDetails = eventDetails;
	}
}
