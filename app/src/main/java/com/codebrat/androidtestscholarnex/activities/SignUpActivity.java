package com.codebrat.androidtestscholarnex.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.codebrat.androidtestscholarnex.R;
import com.codebrat.androidtestscholarnex.utils.SessionManager;

public class SignUpActivity extends AppCompatActivity {

	private static final String TAG = "signup";
	private String phoneRegex = "^[0-9]*$";
	private String emailRegex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	// UI references.
	private AutoCompleteTextView mEmailView;
	private AutoCompleteTextView mUsernameView;
	private AutoCompleteTextView mPhoneNumberView;
	private EditText mPasswordView;
	private TextView mAgreeTermsView;

	private SessionManager mSessionManager;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);
		// Set up the signup form.
		mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
		mUsernameView = (AutoCompleteTextView) findViewById(R.id.username);
		mPhoneNumberView = (AutoCompleteTextView) findViewById(R.id.phonenumber);
		mPasswordView = (EditText) findViewById(R.id.password);
		mAgreeTermsView = (TextView) findViewById(R.id.agree_terms);

		Button mEmailSignUpButton = (Button) findViewById(R.id.email_sign_up_button);

		mSessionManager = new SessionManager(SignUpActivity.this);

		mEmailSignUpButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				attemptRegister();
			}
		});

		TextView tv = (TextView) findViewById(R.id.signup_heading);
		Typeface face = Typeface.createFromAsset(getAssets(),
			"fonts/gotham_htf_bold.ttf");
		tv.setTypeface(face);
		mEmailSignUpButton.setTypeface(face);

		face = Typeface.createFromAsset(getAssets(),
			"fonts/sf_ui_display_medium.ttf");
		mEmailView.setTypeface(face);
		mUsernameView.setTypeface(face);
		mPhoneNumberView.setTypeface(face);
		mPasswordView.setTypeface(face);
		mAgreeTermsView.setTypeface(face);
	}


	/**
	 * Attempts to register the account specified by the register form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	private void attemptRegister() {
		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the register attempt.
		String email = mEmailView.getText().toString();
		String username = mUsernameView.getText().toString();
		String phonenumber = mPhoneNumberView.getText().toString();
		String password = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password, if the user entered one.
		if (!isEmailValid(email)) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		} else if (TextUtils.isEmpty(email)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		} else if(TextUtils.isEmpty(username)) {
			mUsernameView.setError(getString(R.string.error_field_required));
			focusView = mUsernameView;
			cancel = true;
		} else if (!isPhoneValid(phonenumber)){
			mPhoneNumberView.setError(getString(R.string.error_incorrect_phonenumber));
			cancel=true;
		} else if(TextUtils.isEmpty(phonenumber)) {
			mPhoneNumberView.setError(getString(R.string.error_field_required));
			focusView = mPhoneNumberView;
			cancel = true;
		} else if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt registration and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// perform the user register attempt.
			mSessionManager.createLoginSession(username, email, phonenumber);
			Intent intent = new Intent(SignUpActivity.this,MainActivity.class);
			startActivity(intent);
			finish();
		}
	}

	private boolean isEmailValid(String email) {
		return email.matches(emailRegex);
	}

	private boolean isPasswordValid(String password) {
		return password.length() > 4;
	}

	private boolean isPhoneValid(String phonenumber) {
		return phonenumber.matches(phoneRegex);
	}
}
