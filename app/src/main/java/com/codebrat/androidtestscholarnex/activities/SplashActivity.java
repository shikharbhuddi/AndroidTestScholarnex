package com.codebrat.androidtestscholarnex.activities;

import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.codebrat.androidtestscholarnex.R;
import com.codebrat.androidtestscholarnex.utils.SessionManager;

public class SplashActivity extends AppCompatActivity {
	private SessionManager mSessionManager;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		TextView tv = (TextView) findViewById(R.id.splash_head);
		Typeface face = Typeface.createFromAsset(getAssets(),
			"fonts/gotham_htf_bold.ttf");
		tv.setTypeface(face);

		mSessionManager = new SessionManager(SplashActivity.this);

		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				mSessionManager.checkLogin();
			}
		}, 3000);
	}
}
