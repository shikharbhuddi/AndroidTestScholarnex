package com.codebrat.androidtestscholarnex.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.codebrat.androidtestscholarnex.R;
import com.codebrat.androidtestscholarnex.adapter.CustomEventAdapter;
import com.codebrat.androidtestscholarnex.model.EventDataModel;
import com.codebrat.androidtestscholarnex.utils.DatabaseHelper;
import com.codebrat.androidtestscholarnex.utils.SessionManager;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
	implements NavigationView.OnNavigationItemSelectedListener{
	private static final String TAG = MainActivity.class.getSimpleName();
	private SessionManager mSessionManager;
	private String mUsername;
	private String mUserMail;

	private FloatingActionButton mFab;
	private AlertDialog addEventDialog;
	private TextView noEvents;
	private DatabaseHelper databaseHelper;

	ArrayList<EventDataModel> eventDataModels;
	ListView listView;
	private CustomEventAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
			R.string.navigation_drawer_open, R.string.navigation_drawer_close){
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
			}
		};
		drawer.setDrawerListener(toggle);
		toggle.syncState();
		NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);

		mFab = (FloatingActionButton) findViewById(R.id.fab);
		databaseHelper = new DatabaseHelper(MainActivity.this);

		mSessionManager = new SessionManager(MainActivity.this);
		mUsername = mSessionManager.getUserDetails().get("name");
		mUserMail = mSessionManager.getUserDetails().get("email");

		TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
		Typeface face = Typeface.createFromAsset(getAssets(),
			"fonts/gotham_htf_bold.ttf");
		toolbarTitle.setTypeface(face);

		View headerLayout = navigationView.getHeaderView(0);
		TextView userInitials = (TextView) headerLayout.findViewById(R.id.userInitials);
		userInitials.setBackgroundColor(getResources().getColor(R.color.colorAccent));
		userInitials.setText(String.valueOf(mUsername.charAt(0)).toUpperCase());

		TextView userName = (TextView) headerLayout.findViewById(R.id.username);
		TextView userMail = (TextView) headerLayout.findViewById(R.id.usermail);

		userName.setText(mUsername);
		userMail.setText(mUserMail);

		headerLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
				drawer.closeDrawer(GravityCompat.START);
				Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
				startActivity(intent);
			}
		});


		listView=(ListView)findViewById(R.id.listview);

		eventDataModels= new ArrayList<>();

		eventDataModels.addAll(databaseHelper.getAllEventDetails());

		adapter= new CustomEventAdapter(eventDataModels, MainActivity.this);

		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				EventDataModel dataModel= eventDataModels.get(position);;
				Intent intent = new Intent(MainActivity.this, AttendeesActivity.class);
				intent.putExtra("eventName", dataModel.getEventName());
				startActivity(intent);
			}
		});

		mFab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showAddEventDialog();
			}
		});

		noEvents = (TextView) findViewById(R.id.no_events);
		if(eventDataModels.size()==0)
			noEvents.setVisibility(View.VISIBLE);
		else
			noEvents.setVisibility(View.GONE);
	}

	@Override
	public void onBackPressed() {
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		return super.onOptionsItemSelected(item);
	}

	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		// Handle navigation view item clicks here.
		int id = item.getItemId();
		if (id == R.id.nav_logout){
			mSessionManager.logoutUser();
		}
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

	private void showAddEventDialog() {
		addEventDialog = new AlertDialog.Builder(this)
			.setTitle("Add Event")
			.setView(R.layout.dialog_add_event)
			.create();
		addEventDialog.show();
		final EditText nameInput = (EditText) addEventDialog.findViewById(R.id.event_name);

		final EditText detailsInput = (EditText) addEventDialog.findViewById(R.id.event_details);

		Button saveBtn = (Button) addEventDialog.findViewById(R.id.save);

		saveBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String name = nameInput.getText().toString().trim();
				String details = detailsInput.getText().toString().trim();
				String errorMsg = "";

				if (name.isEmpty()) {
					errorMsg = "Name can't be empty";
				} else if (details.isEmpty()) {
					errorMsg = "Details can't be empty";
				}
				if(!errorMsg.isEmpty())
					return;

				if(databaseHelper.addEvent(name, details)==-1)
					Toast.makeText(MainActivity.this, "Error: Can not add same event again.",
						Toast.LENGTH_SHORT).show();

				eventDataModels.clear();
				eventDataModels.addAll(databaseHelper.getAllEventDetails());
				adapter.notifyDataSetChanged();
				noEvents.setVisibility(View.GONE);
				addEventDialog.dismiss();
			}
		});
	}
}
