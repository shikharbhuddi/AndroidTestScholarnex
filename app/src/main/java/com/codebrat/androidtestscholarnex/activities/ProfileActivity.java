package com.codebrat.androidtestscholarnex.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.codebrat.androidtestscholarnex.R;
import com.codebrat.androidtestscholarnex.utils.SessionManager;

import java.util.Calendar;

public class ProfileActivity extends AppCompatActivity {
	private SessionManager mSessionManager;
	AlertDialog editPersonalDetailsDialog = null;
	private String mUsername;
	private String mUserMail;
	private String mUserPhone;

	TextView username;
	TextView usermail;
	TextView userphone;
	TextView userInitials;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		mSessionManager = new SessionManager(ProfileActivity.this);
		mUsername = mSessionManager.getUserDetails().get("name");
		mUserMail = mSessionManager.getUserDetails().get("email");
		mUserPhone = mSessionManager.getUserDetails().get("phonenumber");

		username = (TextView) findViewById(R.id.username);
		usermail = (TextView) findViewById(R.id.usermail);
		userphone = (TextView) findViewById(R.id.phonenumber);
		userInitials = (TextView) findViewById(R.id.userInitials);
		userInitials.setBackgroundColor(getResources().getColor(R.color.colorAccent));
		userInitials.setText(String.valueOf(mUsername.charAt(0)).toUpperCase());

		Typeface face = Typeface.createFromAsset(getAssets(),
			"fonts/sf_ui_display_medium.ttf");
		username.setTypeface(face);
		usermail.setTypeface(face);
		userphone.setTypeface(face);

		face = Typeface.createFromAsset(getAssets(),
			"fonts/gotham_htf_bold.ttf");
		userInitials.setTypeface(face);

		username.setText(mUsername);
		usermail.setText(mUserMail);
		userphone.setText(mUserPhone);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if(id == R.id.action_edit){
			showEditPersonalDetailsDialog();
		}
		return super.onOptionsItemSelected(item);
	}

	private void showEditPersonalDetailsDialog() {
		editPersonalDetailsDialog = new AlertDialog.Builder(this)
			.setTitle("Edit Details")
			.setView(R.layout.dialog_edit_personal_details)
			.create();
		editPersonalDetailsDialog.show();
		final EditText nameInput = (EditText) editPersonalDetailsDialog.findViewById(R.id.edit_name);

        final EditText emailInput = (EditText) editPersonalDetailsDialog.findViewById(R.id.edit_email);

		final EditText phoneInput = (EditText) editPersonalDetailsDialog.findViewById(R.id.edit_phone);
		nameInput.setText(mUsername);

        emailInput.setText(mUserMail);

		phoneInput.setText(mUserPhone);

		Button saveBtn = (Button) editPersonalDetailsDialog.findViewById(R.id.save);

		saveBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String name = nameInput.getText().toString().trim();
				String email = emailInput.getText().toString().trim();
				String phonenumber = phoneInput.getText().toString().trim();
				String errorMsg = "";

				if (name.isEmpty()) {
					errorMsg = "Name can't be empty";
				} else if (email.isEmpty()) {
					errorMsg = "Email can't be empty";
				} else if (phonenumber.isEmpty()) {
					errorMsg = "Phone can't be empty";
				}
				if(!errorMsg.isEmpty())
					return;
				mSessionManager.deleteUserDetails();
				mSessionManager.createLoginSession(name, email, phonenumber);

				editPersonalDetailsDialog.dismiss();

				username.setText(name);
				usermail.setText(email);
				userphone.setText(phonenumber);
				userInitials.setText(String.valueOf((name.charAt(0))).toUpperCase());
			}
		});
	}
}
