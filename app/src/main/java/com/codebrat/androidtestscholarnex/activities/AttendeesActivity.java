package com.codebrat.androidtestscholarnex.activities;

import android.app.Dialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.codebrat.androidtestscholarnex.R;
import com.codebrat.androidtestscholarnex.adapter.CustomAttendeeAdapter;
import com.codebrat.androidtestscholarnex.adapter.CustomEventAdapter;
import com.codebrat.androidtestscholarnex.model.AttendeeDataModel;
import com.codebrat.androidtestscholarnex.utils.DatabaseHelper;

import java.util.ArrayList;

public class AttendeesActivity extends AppCompatActivity {
	private String mEventName;

	private FloatingActionButton mFab;
	private AlertDialog addAttendeeDialog;
	private TextView noAttendees;
	private DatabaseHelper databaseHelper;
	private ArrayList<AttendeeDataModel> attendeeDataModels;
	ListView attendeeListView;
	private CustomAttendeeAdapter adapter;

	private AlertDialog attendenceDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_attendees);
		if(getIntent().getExtras()!=null){
			mEventName = getIntent().getExtras().getString("eventName");
		}
		databaseHelper = new DatabaseHelper(AttendeesActivity.this);

		attendeeListView = (ListView) findViewById(R.id.attendeeListView);
		attendeeDataModels = new ArrayList<>();

		attendeeDataModels.addAll(databaseHelper.getAttendeesDetails(mEventName));

		adapter = new CustomAttendeeAdapter(attendeeDataModels, AttendeesActivity.this);
		attendeeListView.setAdapter(adapter);

		attendeeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				AttendeeDataModel attendeeDataModel = attendeeDataModels.get(position);
				showAttendenceDialog(attendeeDataModel);
			}
		});

		mFab = (FloatingActionButton) findViewById(R.id.fab);
		mFab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showAddAttendeeDialog();
			}
		});

		noAttendees = (TextView) findViewById(R.id.no_attendees);
		if(attendeeDataModels.size()==0)
			noAttendees.setVisibility(View.VISIBLE);
		else
			noAttendees.setVisibility(View.GONE);
	}

	private void showAddAttendeeDialog(){
		addAttendeeDialog = new AlertDialog.Builder(this)
			.setTitle("Add Attendee")
			.setView(R.layout.dialog_add_attendee)
			.create();
		addAttendeeDialog.show();
		final EditText nameInput = (EditText) addAttendeeDialog.findViewById(R.id.attendee_name);

		Button saveBtn = (Button) addAttendeeDialog.findViewById(R.id.save);

		saveBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String name = nameInput.getText().toString().trim();
				String errorMsg = "";

				if (name.isEmpty()) {
					errorMsg = "Name can't be empty";
				}
				if(!errorMsg.isEmpty())
					return;

				if(databaseHelper.addAttendee(name, mEventName)==-1)
					Toast.makeText(AttendeesActivity.this, "Error: Can not add same attendee again.",
						Toast.LENGTH_SHORT).show();

				attendeeDataModels.clear();
				attendeeDataModels.addAll(databaseHelper.getAttendeesDetails(mEventName));
				adapter.notifyDataSetChanged();
				noAttendees.setVisibility(View.GONE);
				addAttendeeDialog.dismiss();
			}
		});
	}

	private void showAttendenceDialog(final AttendeeDataModel attendeeDataModel){
		attendenceDialog = new AlertDialog.Builder(this)
			.setTitle("Mark Attendance")
			.setView(R.layout.dialog_attendance)
			.create();
		attendenceDialog.show();

		final RadioGroup attendenceRadioGroup = (RadioGroup) attendenceDialog.findViewById(R.id.radio_group_attendence);
		RadioButton present = (RadioButton) attendenceDialog.findViewById(R.id.present);
		RadioButton absent = (RadioButton) attendenceDialog.findViewById(R.id.absent);
		RadioButton sick = (RadioButton) attendenceDialog.findViewById(R.id.sick);


		if(attendeeDataModel.getAttendence()!=null){
			switch (attendeeDataModel.getAttendence()){
				case "present": present.setChecked(true);
					break;
				case "absent": absent.setChecked(true);
					break;
				case "sick": sick.setChecked(true);
					break;
			}
		}
		Button saveBtn = (Button) attendenceDialog.findViewById(R.id.save);
		final TextView note = (TextView) attendenceDialog.findViewById(R.id.note);
		final TextView addNote = (TextView) attendenceDialog.findViewById(R.id.add_note);

		if(attendeeDataModel.getNote()!=null && !attendeeDataModel.getNote().equals("")){
			addNote.setVisibility(View.GONE);
			note.setVisibility(View.VISIBLE);
			note.setText(attendeeDataModel.getNote());
		}

		addNote.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addNote.setVisibility(View.GONE);
				note.setVisibility(View.VISIBLE);
			}
		});
		saveBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String errorMsg = "";
				int radioButtonID = attendenceRadioGroup.getCheckedRadioButtonId();
				RadioButton radioButton = (RadioButton)attendenceRadioGroup.findViewById(radioButtonID);
				int idx = attendenceRadioGroup.indexOfChild(radioButton);

				if(radioButtonID==-1){
					errorMsg = "Select atleast one button.";
				}
				if(!errorMsg.isEmpty())
					return;
				attendeeDataModel.setAttendence(radioButton.getText().toString().toLowerCase());
				if(!note.getText().toString().equals("") || note.getText().toString()!=null){
					attendeeDataModel.setNote(note.getText().toString());
				}
				databaseHelper.addAttendence(attendeeDataModel.getAttendeeName(),
					attendeeDataModel.getEventName(), attendeeDataModel.getAttendence(),
					attendeeDataModel.getNote());
				attendeeDataModels.clear();
				attendeeDataModels.addAll(databaseHelper.getAttendeesDetails(mEventName));
				adapter.notifyDataSetChanged();
				attendenceDialog.dismiss();

			}
		});
	}
}
