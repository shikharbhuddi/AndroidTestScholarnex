package com.codebrat.androidtestscholarnex.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.codebrat.androidtestscholarnex.R;
import com.codebrat.androidtestscholarnex.model.AttendeeDataModel;

import java.util.ArrayList;

/**
 * Created by Shikhar on 19-03-2017.
 */

public class CustomAttendeeAdapter extends ArrayAdapter<AttendeeDataModel>{
	private ArrayList<AttendeeDataModel> attendeeDataModelArrayList;
	private static final String TAG = CustomAttendeeAdapter.class.getSimpleName();
	private Context mContext;

	private static class ViewHolder{
		private TextView attendeeName;
		private TextView attendance;
	}

	public CustomAttendeeAdapter(ArrayList<AttendeeDataModel> attendeeDataModels, Context context){
		super(context, R.layout.attendee_list_item, attendeeDataModels);
		attendeeDataModelArrayList = attendeeDataModels;
		mContext = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		AttendeeDataModel attendeeDataModel = getItem(position);

		ViewHolder holder;
		View result;

		if(convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater =
				(LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.attendee_list_item, parent, false);
			holder.attendeeName = (TextView) convertView.findViewById(R.id.attendee_name);
			holder.attendance = (TextView) convertView.findViewById(R.id.attendance);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Typeface face = Typeface.createFromAsset(mContext.getAssets(),
			"fonts/sf_ui_display_medium.ttf");
		holder.attendeeName.setTypeface(face);
		holder.attendeeName.setText(attendeeDataModel.getAttendeeName());
		holder.attendance.setTypeface(face);
		if(attendeeDataModel.getAttendence()!=null && !attendeeDataModel.getAttendence().equals("")){
			holder.attendance.setText(attendeeDataModel.getAttendence().substring(0,1).toUpperCase()
				+ attendeeDataModel.getAttendence().substring(1).toLowerCase());
		}
		result = convertView;
		return result;
	}
}
