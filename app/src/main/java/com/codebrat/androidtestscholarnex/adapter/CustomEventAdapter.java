package com.codebrat.androidtestscholarnex.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.codebrat.androidtestscholarnex.R;
import com.codebrat.androidtestscholarnex.model.EventDataModel;

import java.util.ArrayList;

/**
 * Created by Shikhar on 19-03-2017.
 */

public class CustomEventAdapter extends ArrayAdapter<EventDataModel> {
	private ArrayList<EventDataModel> eventDataModelArrayList;
	private static final String TAG = CustomEventAdapter.class.getSimpleName();
	private Context mContext;

	private static class ViewHolder{
		TextView eventName;
		TextView eventDetails;
	}

	public CustomEventAdapter(ArrayList<EventDataModel> dataModelArrayList, Context context){
		super(context, R.layout.event_list_item, dataModelArrayList);
		eventDataModelArrayList = dataModelArrayList;
		mContext = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		EventDataModel event = getItem(position);

		ViewHolder holder;
		View result;

		if(convertView == null){
			holder = new ViewHolder();
			LayoutInflater inflater =
				(LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.event_list_item, parent, false);
			holder.eventName = (TextView) convertView.findViewById(R.id.event_name);
			holder.eventDetails = (TextView) convertView.findViewById(R.id.event_details);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Typeface face = Typeface.createFromAsset(mContext.getAssets(),
			"fonts/sf_ui_display_medium.ttf");
		holder.eventName.setTypeface(face);
		holder.eventName.setText(event.getEventName());

		holder.eventDetails.setTypeface(face);
		holder.eventDetails.setText(event.getEventDetails());

		result = convertView;
		return result;
	}
}
